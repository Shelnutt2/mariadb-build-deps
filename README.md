# Mariadb Build Deps

This repo contains setup for building a docker image which contains
all mariadb build dependencies. This can be used to reduce the time it takes
for unit tests to run by eliminating the installation of dependencies on each
unit test run.

## Build Docker Image

```
docker built -t registry.gitlab.com/shelnutt2/mariadb-build-deps .
```


## Usage

```
git clone --recursive https://github.com/MariaDB/server.git
cd server
docker run --rm -v $PWD:/opt/server "cmake . && make -j4 && ./mysql-test/mtr main"
```
